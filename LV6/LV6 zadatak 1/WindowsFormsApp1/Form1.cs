﻿//Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
//znanstvenog kalkulatora, odnosno implementirati osnovne (+,-,*,/) i barem 5
//naprednih (sin, cos, log, sqrt...) operacija. 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        double br1, br2;
        public Form1()
        {
            InitializeComponent();
        }

        private void plus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out br1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else if (!double.TryParse(textBox2.Text, out br2))
                MessageBox.Show("Pogresan unos operanda 2!");
            else
            {
                rez.Text = (br1 + br2).ToString();
            }
        }

        private void minus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out br1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else if (!double.TryParse(textBox2.Text, out br2))
                MessageBox.Show("Pogresan unos operanda 2!");
            else
            {
                rez.Text = (br1 - br2).ToString();
            }
        }

        private void pomnozi_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out br1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else if (!double.TryParse(textBox2.Text, out br2))
                MessageBox.Show("Pogresan unos operanda 2!");
            else
            {
                rez.Text = (br1 * br2).ToString();
            }
        }

        private void podijeli_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out br1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else if (!double.TryParse(textBox2.Text, out br2))
                MessageBox.Show("Pogresan unos operanda 2!");
            else
            {
                rez.Text = (br1 / br2).ToString();
            }
        }

        private void sin_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out br1))
                MessageBox.Show("Pogresan unos operanda 1!");
            rez.Text = Math.Sin(br1).ToString();
        }

        private void cos_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out br1))
                MessageBox.Show("Pogresan unos operanda 1!");
            rez.Text = Math.Cos(br1).ToString();
        }

        private void tan_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out br1))
                MessageBox.Show("Pogresan unos operanda 1!");
            rez.Text = Math.Tan(br1).ToString();
        }

        private void ctan_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out br1))
                MessageBox.Show("Pogresan unos operanda 1!");
            rez.Text = Math.Atan(br1).ToString();
        }

        private void sqrt_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out br1))
                MessageBox.Show("Pogresan unos operanda 1!");
            rez.Text = Math.Sqrt(br1).ToString();
        }

        private void Quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
