﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Quit = new System.Windows.Forms.Button();
            this.op1 = new System.Windows.Forms.Label();
            this.op2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.plus = new System.Windows.Forms.Button();
            this.rez = new System.Windows.Forms.Label();
            this.minus = new System.Windows.Forms.Button();
            this.pomnozi = new System.Windows.Forms.Button();
            this.podijeli = new System.Windows.Forms.Button();
            this.sin = new System.Windows.Forms.Button();
            this.cos = new System.Windows.Forms.Button();
            this.tan = new System.Windows.Forms.Button();
            this.ctan = new System.Windows.Forms.Button();
            this.sqrt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Quit
            // 
            this.Quit.Location = new System.Drawing.Point(685, 393);
            this.Quit.Name = "Quit";
            this.Quit.Size = new System.Drawing.Size(86, 34);
            this.Quit.TabIndex = 0;
            this.Quit.Text = "Quit";
            this.Quit.UseVisualStyleBackColor = true;
            this.Quit.Click += new System.EventHandler(this.Quit_Click);
            // 
            // op1
            // 
            this.op1.AutoSize = true;
            this.op1.Location = new System.Drawing.Point(44, 36);
            this.op1.Name = "op1";
            this.op1.Size = new System.Drawing.Size(80, 17);
            this.op1.TabIndex = 1;
            this.op1.Text = "Operand 1:";
            // 
            // op2
            // 
            this.op2.AutoSize = true;
            this.op2.Location = new System.Drawing.Point(44, 78);
            this.op2.Name = "op2";
            this.op2.Size = new System.Drawing.Size(80, 17);
            this.op2.TabIndex = 2;
            this.op2.Text = "Operand 2:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(166, 30);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 3;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(166, 72);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 4;
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(47, 166);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(58, 52);
            this.plus.TabIndex = 5;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // rez
            // 
            this.rez.AutoSize = true;
            this.rez.Location = new System.Drawing.Point(325, 402);
            this.rez.Name = "rez";
            this.rez.Size = new System.Drawing.Size(60, 17);
            this.rez.TabIndex = 6;
            this.rez.Text = "Rezultat";
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(166, 166);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(58, 52);
            this.minus.TabIndex = 7;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // pomnozi
            // 
            this.pomnozi.Location = new System.Drawing.Point(286, 166);
            this.pomnozi.Name = "pomnozi";
            this.pomnozi.Size = new System.Drawing.Size(58, 52);
            this.pomnozi.TabIndex = 8;
            this.pomnozi.Text = "*";
            this.pomnozi.UseVisualStyleBackColor = true;
            this.pomnozi.Click += new System.EventHandler(this.pomnozi_Click);
            // 
            // podijeli
            // 
            this.podijeli.Location = new System.Drawing.Point(400, 166);
            this.podijeli.Name = "podijeli";
            this.podijeli.Size = new System.Drawing.Size(60, 52);
            this.podijeli.TabIndex = 9;
            this.podijeli.Text = "/";
            this.podijeli.UseVisualStyleBackColor = true;
            this.podijeli.Click += new System.EventHandler(this.podijeli_Click);
            // 
            // sin
            // 
            this.sin.Location = new System.Drawing.Point(47, 264);
            this.sin.Name = "sin";
            this.sin.Size = new System.Drawing.Size(58, 56);
            this.sin.TabIndex = 10;
            this.sin.Text = "sin";
            this.sin.UseVisualStyleBackColor = true;
            this.sin.Click += new System.EventHandler(this.sin_Click);
            // 
            // cos
            // 
            this.cos.Location = new System.Drawing.Point(166, 264);
            this.cos.Name = "cos";
            this.cos.Size = new System.Drawing.Size(58, 56);
            this.cos.TabIndex = 11;
            this.cos.Text = "cos";
            this.cos.UseVisualStyleBackColor = true;
            this.cos.Click += new System.EventHandler(this.cos_Click);
            // 
            // tan
            // 
            this.tan.Location = new System.Drawing.Point(286, 264);
            this.tan.Name = "tan";
            this.tan.Size = new System.Drawing.Size(58, 56);
            this.tan.TabIndex = 12;
            this.tan.Text = "tan";
            this.tan.UseVisualStyleBackColor = true;
            this.tan.Click += new System.EventHandler(this.tan_Click);
            // 
            // ctan
            // 
            this.ctan.Location = new System.Drawing.Point(400, 264);
            this.ctan.Name = "ctan";
            this.ctan.Size = new System.Drawing.Size(60, 56);
            this.ctan.TabIndex = 13;
            this.ctan.Text = "ctan";
            this.ctan.UseVisualStyleBackColor = true;
            this.ctan.Click += new System.EventHandler(this.ctan_Click);
            // 
            // sqrt
            // 
            this.sqrt.Location = new System.Drawing.Point(523, 264);
            this.sqrt.Name = "sqrt";
            this.sqrt.Size = new System.Drawing.Size(61, 56);
            this.sqrt.TabIndex = 14;
            this.sqrt.Text = "sqrt";
            this.sqrt.UseVisualStyleBackColor = true;
            this.sqrt.Click += new System.EventHandler(this.sqrt_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.sqrt);
            this.Controls.Add(this.ctan);
            this.Controls.Add(this.tan);
            this.Controls.Add(this.cos);
            this.Controls.Add(this.sin);
            this.Controls.Add(this.podijeli);
            this.Controls.Add(this.pomnozi);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.rez);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.op2);
            this.Controls.Add(this.op1);
            this.Controls.Add(this.Quit);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Quit;
        private System.Windows.Forms.Label op1;
        private System.Windows.Forms.Label op2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Label rez;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button pomnozi;
        private System.Windows.Forms.Button podijeli;
        private System.Windows.Forms.Button sin;
        private System.Windows.Forms.Button cos;
        private System.Windows.Forms.Button tan;
        private System.Windows.Forms.Button ctan;
        private System.Windows.Forms.Button sqrt;
    }
}

