﻿//Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, i u
//svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu
//funkcionalnost koju biste očekivali od takve igre. Nije nužno crtati vješala,
//dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo. 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        Random rand = new Random();
        List<string> wordList = new List<string>();
        int brojPokusaja;
        string RijecIzListe, RijecULabelu;
        string path = "C:\\rijeci\\wordlist.txt";
        public Form1()
        {
            InitializeComponent();
        }

        public void Reset()
        {
            RijecIzListe = wordList[rand.Next(0, wordList.Count - 1)];
            RijecULabelu = new string('*', RijecIzListe.Length);
            textBox1.Text = RijecULabelu;
            brojPokusaja = 5;
            label1.Text = brojPokusaja.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string line;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    wordList.Add(line);
                }
                Reset();
            }
        }

        private void unos_Click(object sender, EventArgs e)
        {
            if (textBox2.Text.Length == 1)
            {
                label2.Text += (textBox2.Text + ", ");
                if (RijecIzListe.Contains(textBox2.Text))
                {
                    string temp_word = RijecIzListe;
                    while (temp_word.Contains(textBox2.Text))
                    {
                        int index = temp_word.IndexOf(textBox2.Text);
                        StringBuilder builder = new StringBuilder

                        (temp_word);

                        builder[index] = '*';
                        temp_word = builder.ToString();
                        StringBuilder builder2 = new StringBuilder

                        (RijecULabelu);

                        builder2[index] = Convert.ToChar(textBox2.Text);
                        RijecULabelu = builder2.ToString();
                    }
                    textBox1.Text = RijecULabelu;
                    if (RijecULabelu == RijecIzListe)
                    {
                        MessageBox.Show("Pobjeda!");
                        Reset();
                    }
                }
                else
                {
                    brojPokusaja--;
                    label1.Text = brojPokusaja.ToString();
                    if (brojPokusaja <= 0)
                    {
                        MessageBox.Show("Kraj igre!");
                        Reset();
                    }

                }
            }
            else if (textBox2.Text.Length > 1)
            {
                if (RijecIzListe == textBox2.Text)
                {
                    MessageBox.Show("Pobjeda!");
                    Reset();
                }
                else
                {
                    brojPokusaja--;
                    label1.Text = brojPokusaja.ToString();
                    if (brojPokusaja <= 0)
                    {
                        MessageBox.Show("Kraj igre!");
                        Reset();
                    }
                }
            }
        }

    private void quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
