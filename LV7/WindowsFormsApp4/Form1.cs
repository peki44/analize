//Napravite igru križić-kružić (iks-oks) korištenjem znanja stečenih na ovoj
//laboratorijskoj vježbi. Omogućiti pokretanje igre, unos imena dvaju igrača, ispis
//koji igrač je trenutno na potezu, igranje igre s iscrtavanjem križića i kružića na
//odgovarajućim mjestima te ispis dijaloga s porukom o pobjedi, odnosno
//neriješenom rezultatu kao i praćenje ukupnog rezultata.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {
        string player1, player2;
        bool turn = true;
        bool check = false;
        int counter = 0;
        int Player1Wins = 0;
        int Player2Wins = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void b1_Click(object sender, EventArgs e)
        {

        }

        private void start_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                player1 = textBox1.Text;
                player2 = textBox2.Text;
                check = true;
                igra.Text = player1;
            }

        }

        private void button_click(object sender, EventArgs e)
        {
            if (check)
            {
                Button b = (Button)sender;
                if (turn)
                {
                    igra.Text = player2;
                    b.Text = "X";
                    turn = !turn;

                }
                else
                {
                    igra.Text = player1;
                    b.Text = "O";
                    turn = !turn;
                }
                b.Enabled = false;
                counter++;
                checkWin();
            }
            else
                MessageBox.Show("Unesite imena igrača i stisnite Pokreni.");

        }
        public void checkWin()
        {
            if ((A1.Text == A2.Text) && (A1.Text == A3.Text) && !A1.Enabled && !A2.Enabled && !A3.Enabled)
            {
                if (turn)
                {
                    MessageBox.Show("Pobjednik je " + player2);
                    Player2Wins++;
                }
                else
                {
                    MessageBox.Show("Pobjednik je " + player1);
                    Player1Wins++;
                }
                Player1Result.Text = Player1Wins.ToString();
                Player2Result.Text = Player2Wins.ToString();
                B1.Enabled = false;
                B2.Enabled = false;
                B3.Enabled = false;
                C1.Enabled = false;
                C2.Enabled = false;
                C3.Enabled = false;
            }
            else if ((B1.Text == B2.Text) && (B1.Text == B3.Text) && !B1.Enabled && !B2.Enabled && !B3.Enabled)
            {
                if (turn)
                {
                    MessageBox.Show("Pobjednik je " + player2);
                    Player2Wins++;
                }
                else
                {
                    MessageBox.Show("Pobjednik je " + player1);
                    Player1Wins++;
                }
                Player1Result.Text = Player1Wins.ToString();
                Player1Result.Text = Player2Wins.ToString();
                A1.Enabled = false;
                A2.Enabled = false;
                A3.Enabled = false;
                C1.Enabled = false;
                C2.Enabled = false;
                C3.Enabled = false;
            }
            else if ((C1.Text == C2.Text) && (C1.Text == C3.Text) && !C1.Enabled && !C2.Enabled && !C3.Enabled)
            {
                if (turn)
                {
                    MessageBox.Show("Pobjednik je " + player2);
                    Player2Wins++;
                }
                else
                {
                    MessageBox.Show("Pobjednik je " + player1);
                    Player1Wins++;
                }
                Player1Result.Text = Player1Wins.ToString();
                Player2Result.Text = Player2Wins.ToString();
                B1.Enabled = false;
                B2.Enabled = false;
                B3.Enabled = false;
                A1.Enabled = false;
                A2.Enabled = false;
                A3.Enabled = false;
            }
            else if ((A1.Text == B1.Text) && (A1.Text == C1.Text) && !A1.Enabled && !B1.Enabled && !C1.Enabled)
            {
                if (turn)
                {
                    MessageBox.Show("Pobjednik je " + player2);
                    Player2Wins++;
                }
                else
                {
                    MessageBox.Show("Pobjednik je " + player1);
                    Player1Wins++;
                }
                Player1Result.Text = Player1Wins.ToString();
                Player2Result.Text = Player2Wins.ToString();
                A2.Enabled = false;
                B2.Enabled = false;
                C2.Enabled = false;
                A3.Enabled = false;
                B3.Enabled = false;
                C3.Enabled = false;
            }
            else if ((A2.Text == B2.Text) && (A2.Text == C2.Text) && !A2.Enabled && !B2.Enabled && !C2.Enabled)
            {
                if (turn)
                {
                    MessageBox.Show("Pobjednik je " + player2);
                    Player2Wins++;
                }
                else
                {
                    MessageBox.Show("Pobjednik je " + player1);
                    Player1Wins++;
                }
                Player1Result.Text = Player1Wins.ToString();
                Player2Result.Text = Player2Wins.ToString();
                A1.Enabled = false;
                B1.Enabled = false;
                C1.Enabled = false;
                A3.Enabled = false;
                B3.Enabled = false;
                C3.Enabled = false;
            }
            else if ((A3.Text == B3.Text) && (A3.Text == C3.Text) && !A3.Enabled && !B3.Enabled && !C3.Enabled)
            {
                if (turn)
                {
                    MessageBox.Show("Pobjednik je " + player2);
                    Player2Wins++;
                }
                else
                {
                    MessageBox.Show("Pobjednik je " + player1);
                    Player1Wins++;
                }
                Player1Result.Text = Player1Wins.ToString();
                Player2Result.Text = Player2Wins.ToString();
                A2.Enabled = false;
                B2.Enabled = false;
                C2.Enabled = false;
                A1.Enabled = false;
                B1.Enabled = false;
                C1.Enabled = false;
            }
            else if ((A1.Text == B2.Text) && (A1.Text == C3.Text) && !A1.Enabled && !B2.Enabled && !C3.Enabled)
            {
                if (turn)
                {
                    MessageBox.Show("Pobjednik je " + player2);
                    Player2Wins++;
                }
                else
                {
                    MessageBox.Show("Pobjednik je " + player1);
                    Player1Wins++;
                }
                Player1Result.Text = Player1Wins.ToString();
                Player2Result.Text = Player2Wins.ToString();
                A2.Enabled = false;
                A3.Enabled = false;
                B1.Enabled = false;
                B3.Enabled = false;
                C1.Enabled = false;
                C2.Enabled = false;
            }
            else if ((A3.Text == B2.Text) && (A3.Text == C1.Text) && !A3.Enabled && !B2.Enabled && !C1.Enabled)
            {
                if (turn)
                {
                    MessageBox.Show("Pobjednik je " + player2);
                    Player2Wins++;
                }
                else
                {
                    MessageBox.Show("Pobjednik je " + player1);
                    Player1Wins++;
                }
                Player1Result.Text = Player1Wins.ToString();
                Player2Result.Text = Player2Wins.ToString();
                A1.Enabled = false;
                A2.Enabled = false;
                B1.Enabled = false;
                B3.Enabled = false;
                C2.Enabled = false;
                C3.Enabled = false;
            }
            else if (counter == 9)
                MessageBox.Show("Nerijeseno je ");


        }

        private void restart_Click(object sender, EventArgs e)
        {
            counter = 0;
            A1.Enabled = true;
            A2.Enabled = true;
            A3.Enabled = true;
            B1.Enabled = true;
            B2.Enabled = true;
            B3.Enabled = true;
            C1.Enabled = true;
            C2.Enabled = true;
            C3.Enabled = true;
            A1.Text = "";
            A2.Text = "";
            A3.Text = "";
            B1.Text = "";
            B2.Text = "";
            B3.Text = "";
            C1.Text = "";
            C2.Text = "";
            C3.Text = "";

        }

        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
